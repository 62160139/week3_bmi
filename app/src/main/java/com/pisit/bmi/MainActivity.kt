package com.pisit.bmi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.pisit.bmi.databinding.ActivityMainBinding
import kotlin.math.ceil
import kotlin.math.pow

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener{ calculateTip() }
    }

    fun calculateTip() {
        val stringInWeight = binding.weight.text.toString()
        val stringInHeight = binding.height.text.toString()
        val weight = stringInWeight.toDoubleOrNull()
        val height = stringInHeight.toDoubleOrNull()
        if (weight == null || weight == 0.0 || height == null || height == 0.0) {
            return
        }
        var bmi = weight / ((height/100).pow(2))
        bmi = ceil(bmi)
        binding.bmiCalculate.text = getString(R.string.bmi_calculate, bmi.toString())
        var type = if (bmi < 18.5) {
            "UNDERWEIGHT"
        }else if (bmi in 18.5..24.9){
            "NORMAL"
        }else if (bmi in 25.0..29.9){
            "OVERWEIGHT"
        }else{
            "OBESE"
        }
        binding.typeResult.text = type
    }
    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}